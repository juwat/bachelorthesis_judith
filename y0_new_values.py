#!/usr/bin/env python
# coding: utf-8

# In[1]:


get_ipython().run_line_magic('matplotlib', 'inline')

import matplotlib.pyplot as plt
import numpy as np
import re as re

import seaborn as sns; sns.set()

from modelbase.ode import Model, Simulator

import copy


# In[2]:


from model import model as m


# In[6]:


def calculate_pHinv(x):
    return (4e3*10**(-x))


# In[3]:


variables = [
        #"B",  #photosystem II protein concentration
        "PQ",  # oxidised plastoquinone
        "PC",  # oxidised plastocyan
        "Fd",  # oxidised ferrodoxin
        "ATP",  # stromal concentration of ATP
        "NADPH",  # stromal concentration of NADPH
        "H",  # lumenal protons
        "LHC",#,  # non-phosphorylated antenna
        "Psbs", # PsBs
        "Vx",  #vioolaxathin relative concentration
        "PGA", 
        'BPGA', 
        'GAP', 
        'DHAP', 
        'FBP', 
        'F6P', 
        'G6P', 
        'G1P', 
        'SBP', 
        'S7P', 
        'E4P', 
        'X5P', 
        'R5P', 
        'RUBP', 
        'RU5P'
]


# In[4]:


m.get_stoichiometric_df()


# In[7]:


s = Simulator(m)

# Set initial conditions using dictionary
y0 = {
        "PQ":m.get_parameter('PQtot')/2,
        "PC":m.get_parameter('PCtot')/2,
        "Fd":m.get_parameter('Fdtot')/2,
        "ATP":0.12,
        "NADPH":0.281543418344,
        "H":calculate_pHinv(7.2),
        "LHC":0.9,
        "Psbs":0.9,
        "Vx":0.9,
        "PGA":2.15,
        'BPGA':0.000907499521924,
        'GAP':0.011839616887,
        'DHAP':0.260471552645,
        'FBP':0.11,
        'F6P':0.36,
        'G6P':0.46,
        'G1P':0.166166918189,
        'SBP':0.2,
        'S7P':0.56,
        'E4P':0.0330766864679,
        'X5P':0.0374527459593,
        'R5P':0.0627333486958,
        'RUBP':0.45,
        'RU5P':0.02,
}


# In[8]:


# adjusted the State 2 values of LHC, Psbs and Vx
y02 = {
        "PQ":m.get_parameter('PQtot')/2,
        "PC":m.get_parameter('PCtot')/2,
        "Fd":m.get_parameter('Fdtot')/2,
        "ATP":0.12,
        "NADPH":0.281543418344,
        "H":calculate_pHinv(7.2),
        "LHC":0.0,
        "Psbs":0.45,
        "Vx":0.45,
        "PGA":2.15,
        'BPGA':0.000907499521924,
        'GAP':0.011839616887,
        'DHAP':0.260471552645,
        'FBP':0.11,
        'F6P':0.36,
        'G6P':0.46,
        'G1P':0.166166918189,
        'SBP':0.2,
        'S7P':0.56,
        'E4P':0.0330766864679,
        'X5P':0.0374527459593,
        'R5P':0.0627333486958,
        'RUBP':0.45,
        'RU5P':0.02,
}


# In[9]:


s.initialise(y0)
s.simulate(1400)


# In[10]:


s.get_results_array()


# In[11]:


s = Simulator(m)

s.update_parameters({'pfd': 0.0, "kProtonationL": 0.0, "kDeprotonation": 0.0, "kDeepoxV": 0.0, "kEpoxZ": 0.0})

s.initialise(y0)
s.simulate(1400, steps=5000, **{'atol':1.e-10}) #time_points=None

y0dark12 = s.get_results_array()[-1]


# In[12]:


y0dark12 = np.array(y0dark12)


# In[13]:


y0_ref = np.array([1.70731708e+01, 2.24893548e-04, 4.99994735e+00, 1.08333440e-07,
 6.22469064e-08, 5.03857184e-05, 9.00923300e-01, 9.00000000e-01,
 9.00000000e-01, 4.28050685e-09, 5.64570919e-20, 8.91461011e-13,
 1.96121420e-11, 1.24133500e-22, 3.65029174e-01, 8.39567101e-01,
 4.86948918e-02, 1.50771960e-17, 4.72289336e-01, 5.91359605e-08,
 4.62229481e-07, 7.74234381e-07, 4.20781643e-13, 3.09693752e-07])


# In[14]:


y0dark12 == y0_ref


# In[15]:


s.plot_selection('PQ')


# In[16]:


print(y0dark12)


# In[17]:


s = Simulator(m)

s.update_parameters({'pfd': 100.0, "kProtonationL": 0.0096, "kDeprotonation": 0.0096, "kDeepoxV": 0.0024, "kEpoxZ": 0.00024})
s.update_parameters({'pfd': 0.0, "kProtonationL": 0.0, "kDeprotonation": 0.0, "kDeepoxV": 0.0, "kEpoxZ": 0.0, "ox": False})

s.initialise(y02)
s.simulate(1200, steps=5000, **{'atol':1.e-8}) # values > 1200 give error, upgrading steps didn't work

y0dark2 = s.get_results_array()[-1]


# In[18]:


print(y0dark2)


# In[19]:


s = Simulator(m)

s.update_parameters({"pfd": 100., "kProtonationL": 0.0096, "kDeprotonation": 0.0096, "kDeepoxV": 0.0024, "kEpoxZ": 0.00024, "ox": True})
s.update_parameter('pfd', 0.0)

s.initialise(y02)
s.simulate(1200, steps=3000, **{'atol':1.e-10}) # same problem

y0high2 = s.get_results_array()[-1]


# In[20]:


print(y0high2)

