import matplotlib.pyplot as plt
import numpy as np

from modelbase.ode import Simulator

from model import model as m
from reactionrates import calculate_pHinv as phinv
from modelbase.utils.plotting import plot_grid


# Set initial conditions using dictionary
y0 = {
        "PQ":m.get_parameter('PQtot')/2,
        "PC":m.get_parameter('PCtot')/2,
        "Fd":m.get_parameter('Fdtot')/2,
        "ATP":0.12,
        "NADPH":0.281543418344,
        "H":phinv(7.2),
        "LHC":0.9,
        "Psbs":0.9,
        "Vx":0.9,
        "PGA":2.15,
        'BPGA':0.000907499521924,
        'GAP':0.011839616887,
        'DHAP':0.260471552645,
        'FBP':0.11,
        'F6P':0.36,
        'G6P':0.46,
        'G1P':0.166166918189,
        'SBP':0.2,
        'S7P':0.56,
        'E4P':0.0330766864679,
        'X5P':0.0374527459593,
        'R5P':0.0627333486958,
        'RUBP':0.45,
        'RU5P':0.02,
}

s = Simulator(m)
s.initialise(y0)
t, y = s.simulate(100)

groups = [
    ["PQ"],
    ["PC","Fd"],
]

s.plot_grid(
    compound_groups=groups,
    ncols=2,
    sharex=True,
    sharey=False,
    xlabels="Time [au]",
    ylabels="Concentration [mmol/mol Chl]",
    figure_title="Electron transport chain electron carriers concentrations",
)

groups = [
    ["G6P", "PGA", "F6P", "S7P"],
    ["RU5P", "X5P", "SBP", "G1P"],
    ["ATP", "DHAP", "RUBP", "R5P"],
    ["BPGA", "E4P", "FBP", "GAP"],
]

s.plot_grid(
    compound_groups=groups,
    ncols=2,
    sharex=True,
    sharey=False,
    xlabels="Time [au]",
    ylabels="Concentration [mM]",
    figure_title="Calvin cycle substrate concentrations",
)
plt.show()

