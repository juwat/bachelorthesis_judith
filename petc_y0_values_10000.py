#!/usr/bin/env python
# coding: utf-8

# ###  Photosynthetic Electron Transport Chain mathematical model (version from 2014)
# With this Notebook you can reproduce the results presented in 
# 
# Oliver Ebenhöh, Geoffrey Fucile, Giovanni Finazzi, Jean‐David Rochaix and Michel Goldschmidt‐Clermont (2014)
# "Short-term acclimation of the photosynthetic electron transfer chain to changing light: a mathematical model"
# *Phil Trans Roy Soc B* 369 doi:10.1098/rstb.2013.0223
# 
# The mathematical model of the photosynthetic electron transport chain has been implemented using the modelbase1.0 software developed at the University of Dusseldorf.

# In[1]:


# I want to have my figures active here (e.g., zoom into my plots)
get_ipython().run_line_magic('matplotlib', 'notebook')


# In[2]:


globals()


# In[3]:


locals()


# In[4]:


dir()


# In[5]:


from modelbase.ode import Model, Simulator

import numpy as np
import matplotlib.pyplot as plt


# In[6]:


import petc_model_file


# In[7]:


globals()


# In[8]:


locals()


# In[9]:


dir()


# In[10]:


petc_model_file.m.get_stoichiometric_df()


# In[11]:


# add two new compounds
petc_model_file.m.add_compounds(['Vx', 'Psbs'])

# add two pools of the two compounds as parameters
petc_model_file.m.add_parameters({"Psbstot": 1., # relative pool of PsbS
     "Xtot": 1. # relative pool of carotenoids (V+A+Z)]
                  })

# set the derived compounds based on the conservation 
def xmoiety(Vx, Xtot):
    return Xtot - Vx

petc_model_file.m.add_algebraic_module(
    module_name="xantophylls_alm",
    function=xmoiety,
    compounds=["Vx"],
    derived_compounds=["Zx"],
    parameters=['Xtot']
)

def psbsmoiety(Psbs, Psbstot):
    return Psbstot - Psbs

petc_model_file.m.add_algebraic_module(
    module_name="psbs_alm",
    function=psbsmoiety,
    compounds=["Psbs"],
    derived_compounds=["Psbsp"],
    parameters=['Psbstot']
)

# add the four reactions and parameters needed to run it
petc_model_file.m.add_parameters({ # quencher fitted parameters
     "gamma0": 0.1,          # slow quenching of Vx present despite lack of protonation
     "gamma1": 0.25,         # fast quenching present due to the protonation
     "gamma2": 0.6,          # slow quenching of Zx present despite lack of protonation
     "gamma3": 0.15,         # fastest possible quenching

#     # non-photochemical quenching PROTONATION
     "kDeprotonation": 0.0096,
     "kProtonationL": 0.0096,
     "kphSatLHC": 5.8,
     #"nH": 5.,
     #"NPQsw": 5.8,

#     # non-photochemical quenching XANTOPHYLLS
     "kDeepoxV": 0.0024,
     "kEpoxZ": 0.00024,      # 6.e-4        # converted to [1/s]
     "kphSat": 5.8,          # [-] half-saturation pH value for activity de-epoxidase highest activity at ~pH 5.8
     "kHillX": 5.,     # [-] hill-coefficient for activity of de-epoxidase
     "kHillL": 3.,     # [-] hill-coefficient for activity of de-epoxidase
     "kZSat": 0.12,          # [-] half-saturation constant (relative conc. of Z) for quenching of Z
 })

def vDeepox(Vx, H, nH, kDeepoxV, kphSat):
    """
    activity of xantophyll cycle: de-epoxidation of violaxanthin, modelled by Hill kinetics
    """
    vf = kDeepoxV * ((H ** nH)/ (H ** nH + calculate_pHinv(kphSat) ** nH)) * Vx
    return vf

petc_model_file.m.add_reaction(
    rate_name='vDeepox',
    function=vDeepox,
    stoichiometry= {"Vx": -1},
    modifiers=["H"],
    parameters=["kHillX", "kDeepoxV", "kphSat"],
    reversible=False,
)

def vEpox(Zx, kEpoxZ):
    """
    activity of xantophyll cycle: epoxidation
    """
    vr = kEpoxZ * Zx
    return vr

petc_model_file.m.add_reaction(
    rate_name='vEpox',
    function=vEpox,
    stoichiometry= {"Vx": 1},
    dynamic_variables=["Zx"],
    parameters=["kEpoxZ"],
    reversible=True,
)

def vLhcprotonation(Psbs, H, nH, kProtonationL, kphSatLHC):
    """
    activity of PsbS protein protonation: protonation modelled by Hill kinetics
    """
    vf = kProtonationL * ((H ** nH)/ (H ** nH + calculate_pHinv(kphSatLHC) ** nH)) * Psbs
    return vf

petc_model_file.m.add_reaction(
    rate_name='vLhcprotonation',
    function=vLhcprotonation,
    stoichiometry= {"Psbs": -1},
    modifiers=["H"],
    parameters=["kHillL", "kProtonationL", "kphSatLHC"],
    reversible=False,
)

def vLhcdeprotonation(Psbsp, kDeprotonation):
    """
    activity of PsbS protein protonation: deprotonation
    """
    vr = kDeprotonation * Psbsp
    return vr

petc_model_file.m.add_reaction(
    rate_name='vLhcdeprotonation',
    function=vLhcdeprotonation,
    stoichiometry= {"Psbs": 1},
    dynamic_variables=["Psbsp"],
    parameters=["kDeprotonation"],
    reversible=True,
)


# In[12]:


petc_model_file.m.get_stoichiometric_df()


# In[13]:


def quencher(Psbs, Vx, Psbsp, Zx, y0, y1, y2, y3, kZSat):
    """ 
    co-operatiove 4-state quenching mechanism
    
    Comment:
        seems like this can't reach 1, so I've checked the max value and normalized it
        for i in np.linspace(0,1,100):
            for j in np.linspace(0,1,100):
                a = np.hstack((a, vQuencher4states(p, i, j)))
        max(a)
    """
    ZAnt = Zx / (Zx + kZSat)
    Q = (y0 * Vx * Psbs + y1 * Vx * Psbsp + y2 * ZAnt * Psbsp + y3 * ZAnt * Psbs)#/0.5625
    #Q = (y0 * (1-ZAnt) * Psbs + y1 * (1-ZAnt) * Psbsp + y2 * ZAnt * Psbsp + y3 * ZAnt * Psbs)#/0.5625
    return Q

petc_model_file.m.add_algebraic_module(
    module_name="quencher",
    function=quencher,
    compounds=["Psbs", "Vx", "Psbsp", "Zx"],
    derived_compounds=["Q"],
    modifiers=None,
    parameters=["gamma0", "gamma1", "gamma2", "gamma3", "kZSat"],
)


# In[14]:


petc_model_file.m.get_stoichiometric_df()


# In[15]:


petc_model_file.m.get_derived_compounds()


# In[16]:


def ps2states(PQ, PQred, ps2cs, Q, PSIItot, k2, kF, _kH, kH0, Keq_PQred, kPQred, pfd):
    L = ps2cs * pfd
    kH = kH0 + _kH * Q
    k3p = kPQred * PQ
    k3m = kPQred * PQred / Keq_PQred
   
    Bs = []

    
    if isinstance(kH, float) and isinstance(PQ, np.ndarray):
        kH = np.repeat(kH, len(PQ))
      
    
    for L, kH, k3p, k3m in zip(L, kH, k3p, k3m):
        M = np.array(
            [
                [-L - k3m, kH + kF, k3p, 0],
                [L, -(kH + kF + k2), 0, 0],
                [0, 0, L, -(kH + kF)],
                [1, 1, 1, 1],
            ]
        )
        A = np.array([0, 0, 0, PSIItot])
        B0, B1, B2, B3 = np.linalg.solve(M, A)
        Bs.append([B0, B1, B2, B3])
    return np.array(Bs).T


petc_model_file.m.add_algebraic_module(
    module_name="ps2states",
    function=ps2states,
    compounds=["PQ", "PQred", "ps2cs", "Q"],
    derived_compounds=["B0", "B1", "B2", "B3"],
    parameters=["PSIItot", "k2", "kF", "kH", "kH0", "Keq_PQred", "kPQred", "pfd"],
)

def fluorescence(Q, B0, B2, ps2cs, k2, kF, kH, kH0):
    fluo = (ps2cs * kF * B0) / (kF + kH0 + k2 + kH * Q) + (ps2cs * kF * B2) / (kF + kH0+ kH * Q)
    return fluo

petc_model_file.m.add_algebraic_module(
    module_name="fluorescence",
    function=fluorescence,
    compounds=["Q", "B0", "B2", "ps2cs"],
    derived_compounds=["Fluo"],
    modifiers=None,
    parameters=["k2","kF", "kH", "kH0"]
)


# In[17]:


from petc_model_file import calculate_pHinv


# In[18]:


calculate_pHinv


# In[19]:


s = Simulator(petc_model_file.m)


# In[20]:


y0 = {
        "PQ":petc_model_file.m.get_parameter('PQtot')/2,
        "PC":petc_model_file.m.get_parameter('PCtot')/2,
        "Fd":petc_model_file.m.get_parameter('Fdtot')/2,
        "ATP":0.12,
        "NADPH":0.281543418344,
        "H":calculate_pHinv(7.2),
        "LHC":0.9,
        "Psbs":0.9,
        "Vx":0.9,
}


# In[21]:


y02 = {
        "PQ":petc_model_file.m.get_parameter('PQtot')/2,
        "PC":petc_model_file.m.get_parameter('PCtot')/2,
        "Fd":petc_model_file.m.get_parameter('Fdtot')/2,
        "ATP":0.12,
        "NADPH":0.281543418344,
        "H":calculate_pHinv(7.2),
        "LHC":0.45,
        "Psbs":0.45,
        "Vx":0.45,
}


# In[22]:


s.initialise(y0)
s.simulate(10000)


# In[23]:


s = Simulator(petc_model_file.m)

s.update_parameters({'pfd': 0.0, "kProtonationL": 0.0, "kDeprotonation": 0.0, "kDeepoxV": 0.0, "kEpoxZ": 0.0})

s.initialise(y0)
s.simulate(10000, steps=10000, **{'atol':1.e-20}) #time_points=None

y0dark12_petc = s.get_results_array()[-1]

s.plot_selection('PQ')
plt.title("Plastoquinone (y0dark12_petc)")
plt.xlabel("Time [sec]")
plt.ylabel("PQ")
plt.legend()
plt.show()
plt.tight_layout()


# In[36]:


s = Simulator(petc_model_file.m)

#s.update_parameters({'pfd': 100.0, "kProtonationL": 0.0096, "kDeprotonation": 0.0096, "kDeepoxV": 0.0024, "kEpoxZ": 0.00024})
s.update_parameters({'pfd': 0.0, "kProtonationL": 0.0, "kDeprotonation": 0.0, "kDeepoxV": 0.0, "kEpoxZ": 0.0, "ox": False, "Ton": 0.0, "Toff":10000.0})

s.initialise(y02)
s.simulate(10000, steps=10000, **{'atol':1.e-20}) # values > 1200 give error, upgrading steps didn't work

y0dark2_petc = s.get_results_array()[-1]

s.plot_selection('H')
plt.title("Plastoquinone (y0dark2_petc)")
plt.xlabel("Time [sec]")
plt.ylabel("PQ")
plt.legend()
plt.show()
plt.tight_layout()


# In[37]:


s = Simulator(petc_model_file.m)

s.update_parameters({"pfd": 100., "kProtonationL": 0.0096, "kDeprotonation": 0.0096, "kDeepoxV": 0.0024, "kEpoxZ": 0.00024, "ox": True})
s.update_parameter('pfd', 0.0)

s.initialise(y02)
s.simulate(10000, steps=10000, **{'atol':1.e-20}) # same problem

y0high2_petc = s.get_results_array()[-1]

s.plot_selection('PQ')
plt.title("Plastoquinone (y0high2_petc)")
plt.xlabel("Time [sec]")
plt.ylabel("PQ")
plt.legend()
plt.show()
plt.tight_layout()


# In[38]:


print(y0dark12_petc)
print(y0dark2_petc)
print(y0high2_petc)

